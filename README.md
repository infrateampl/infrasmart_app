# infraAPP (infrasmart_app)

Application for infraTEAM Conference Participants

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Ważne linki
```
https://quasar.dev/quasar-cli/developing-capacitor-apps/capacitor-api
Dev:
quasar dev -m capacitor -T [ios|android]
Prod:
quasar build -m capacitor -T [ios|android]
Google analytics:
https://quasar.dev/quasar-cli/developing-capacitor-apps/managing-google-analytics
Publishing to app stores:
https://quasar.dev/quasar-cli/developing-capacitor-apps/publishing-to-store
```
