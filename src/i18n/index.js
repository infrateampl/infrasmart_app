import pl from './pl'
import en from './en'

export default {
  pl: pl,
  en: en
}
