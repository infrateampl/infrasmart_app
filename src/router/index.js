import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'

export default route(function ({ store, ssrContext }) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
  })

  Router.beforeEach(async (to, from, next) => {
    if (
      to.meta.loggedIn === true && !store.getters['auth/getLoggedIn'] // if required logged in but user not logged in
    ) {
      return next({ name: 'login' })
    }
    if (
      to.meta.loggedIn === false && store.getters['auth/getLoggedIn'] // if required not logged in but user logged in
    ) {
      return next({ name: 'home' })
    }
    if (
      to.meta.roles // if required to has roles
    ) {
      const userData = store.getters['auth/getUserData']
      let ok = false
      to.meta.roles.forEach(value => {
        if (userData.roles[value]) {
          ok = true
        }
      })
      if (!ok) {
        return next({ name: 'error-403' })
      }
    }
    return next()
  })

  return Router
})
