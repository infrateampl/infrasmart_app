
const routes = [
  {
    path: '/',
    component: () => import('layouts/PreLoginLayout.vue'),
    children: [
      {
        name: 'login.welcome',
        path: '',
        component: () => import('pages/Auth/Welcome.vue'),
        meta: {
          loggedIn: false
        }
      },
      {
        name: 'register',
        path: 'register',
        component: () => import('pages/Auth/Register.vue'),
        meta: {
          loggedIn: false
        }
      },
      {
        name: 'login',
        path: 'login',
        component: () => import('pages/Auth/Login.vue'),
        meta: {
          loggedIn: false
        }
      },
      {
        name: 'register.fill_data',
        path: 'register/fill_data',
        component: () => import('pages/Auth/DataFill.vue'),
        meta: {
          loggedIn: true
        }
      },
      {
        name: 'register.fill_data_success',
        path: 'register/fill_data/success',
        component: () => import('pages/Auth/DataFillSuccess.vue'),
        meta: {
          loggedIn: true
        }
      },
      {
        name: 'login.passcode',
        path: 'login/passcode/:login_method/:user',
        component: () => import('pages/Auth/LoginPassCode.vue'),
        meta: {
          loggedIn: false
        }
      },
      {
        path: '/ticket/method_choice',
        name: 'ticket.method',
        component: () => import('pages/Ticket/MethodChoice.vue'),
        meta: {
          loggedIn: true,
          roles: [5]
        }
      },
      {
        path: '/ticket/pricing',
        name: 'ticket.pricing',
        component: () => import('pages/Ticket/List.vue'),
        meta: {
          loggedIn: true,
          roles: [5]
        }
      },
      {
        path: '/ticket/buyer_data/:ticket_type',
        name: 'ticket.buyer_data',
        component: () => import('pages/Ticket/BuyerData.vue'),
        meta: {
          loggedIn: true,
          roles: [5]
        }
      },
      {
        path: '/ticket/sum_up',
        name: 'ticket.sum_up',
        component: () => import('pages/Ticket/SumUp.vue'),
        meta: {
          loggedIn: true,
          roles: [5]
        }
      },
      {
        path: '/ticket/voucher',
        name: 'ticket.voucher',
        component: () => import('pages/Ticket/Voucher.vue'),
        meta: {
          loggedIn: true,
          roles: [5]
        }
      },
      {
        path: '/ticket/payment/paid/:invoice_id/:payment_id',
        name: 'ticket.payment.paid',
        component: () => import('pages/Ticket/AfterPayment.vue'),
        meta: {
          loggedIn: true
        }
      }
    ]
  },
  {
    path: '/logged_in',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'home',
        path: '/logged_in/',
        component: () => import('pages/Index.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.staff',
        path: '/logged_in/staff',
        component: () => import('pages/Staff/StaffActions.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.scanner',
        path: '/logged_in/scanner',
        component: () => import('pages/Staff/TicketScanner'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.map',
        path: '/logged_in/map',
        component: () => import('pages/Map.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'user.edit',
        path: '/logged_in/user/edit',
        component: () => import('pages/User/Edit.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.ticket',
        path: '/logged_in/ticket',
        component: () => import('pages/Ticket/QR.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.agenda',
        path: '/logged_in/agenda',
        component: () => import('pages/Agenda.vue'),
        meta: {
          roles: [4]
        }
      },
      {
        name: 'main.userData',
        path: '/logged_in/user_data',
        component: () => import('pages/User/UserData.vue'),
        meta: {
          roles: [4]
        }
      }
    ],
    meta: {
      loggedIn: true
    }
  },
  {
    name: 'error-403',
    path: '/error-403',
    component: () => import('pages/Error403.vue')
  },
  {
    name: 'error.404',
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
