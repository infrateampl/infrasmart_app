export function getLoggedIn (state) {
  const { loggedIn } = state
  if (JSON.parse(window.localStorage.getItem('loggedIn')) !== JSON.parse(loggedIn) && JSON.parse(window.localStorage.getItem('loggedIn')) === false) {
    // commit('standardAuth/setLoggedOut')
    return false
  } if (JSON.parse(window.localStorage.getItem('loggedIn')) !== JSON.parse(loggedIn) && JSON.parse(window.localStorage.getItem('loggedIn')) === true) {
    return true
  }
  return JSON.parse(loggedIn)
}

export function getUserId (state) {
  // eslint-disable-next-line camelcase
  let { user_id } = state
  // eslint-disable-next-line camelcase
  if (JSON.parse(user_id) === 0) {
    // eslint-disable-next-line camelcase
    user_id = window.localStorage.getItem('user_id') ?? user_id
  }
  // eslint-disable-next-line camelcase
  return JSON.parse(user_id)
}

export function getTicketId (state) {
  // eslint-disable-next-line camelcase
  let { ticket_id } = state
  // eslint-disable-next-line camelcase
  if (JSON.parse(ticket_id) === 0) {
    // eslint-disable-next-line camelcase
    ticket_id = window.localStorage.getItem('ticket_id') ?? ticket_id
  }
  // eslint-disable-next-line camelcase
  return JSON.parse(ticket_id)
}

export function getUserData (state) {
  let { userData } = state
  if (userData === '') {
    userData = window.localStorage.getItem('userData') ?? userData
  }
  return JSON.parse(userData)
}

export function getSessionId (state) {
  let { loggedIn } = state
  loggedIn = JSON.parse(loggedIn)
  if (JSON.parse(window.localStorage.getItem('loggedIn')) !== loggedIn && JSON.parse(window.localStorage.getItem('loggedIn')) === false) {
    loggedIn = false
  } else if (JSON.parse(window.localStorage.getItem('loggedIn')) !== loggedIn && JSON.parse(window.localStorage.getItem('loggedIn')) === true) {
    loggedIn = true
  }
  if (loggedIn) {
    let { sessionId } = state
    if (sessionId.length === 0 || (sessionId.length > 0 && JSON.parse(sessionId) === '')) {
      sessionId = window.localStorage.getItem('sessionId') ?? sessionId
    }
    return JSON.parse(sessionId)
  }
  return ''
}

export function getBearerToken (state) {
  let { loggedIn } = state
  loggedIn = JSON.parse(loggedIn)
  if (JSON.parse(window.localStorage.getItem('loggedIn')) !== loggedIn && JSON.parse(window.localStorage.getItem('loggedIn')) === false) {
    loggedIn = false
  } else if (JSON.parse(window.localStorage.getItem('loggedIn')) !== loggedIn && JSON.parse(window.localStorage.getItem('loggedIn')) === true) {
    loggedIn = true
  }

  if (loggedIn) {
    let { bearerToken } = state
    if ((bearerToken && JSON.parse(bearerToken) === '') || !bearerToken) {
      bearerToken = window.localStorage.getItem('bearerToken') ?? bearerToken
    }
    return JSON.parse(bearerToken)
  }
  return ''
}
