import { api } from 'boot/axios'

export const login = ({ commit }, payload) => {
  return api.post('/api/system/auth/standard/login', payload)
    .then(res => {
      if (res.data.success) {
        commit('setLoggedIn')
        commit('updateUserId', res.data.data.user_id)
        commit('setSessionId', res.data.data.sessionId)
        commit('setBearerToken', res.data.data.token)
      }
    })
}

export const hardLogin = ({ commit }, payload) => {
  commit('setLoggedIn')
  commit('updateUserId', payload.user_id)
  commit('setSessionId', payload.sessionId)
  commit('setBearerToken', payload.token)
}

export const logout = ({ commit, getters }) => {
  return api.get('/api/system/auth/standard/logout', {
    headers: {
      Authorization: `Bearer ${getters.getBearerToken}`,
      LoginSessionId: 'asdasdasd',
      Accept: 'application/json'
    }
  })
    .then(() => {
      commit('setLoggedOut')
    })
}

export const loadUserData = ({ commit, dispatch, getters }, payload) => {
  return api.get('/api/system/auth/standard/user_data', {
    headers: {
      Authorization: `Bearer ${getters.getBearerToken}`,
      'Accept-Language': payload.locale,
      LoginSessionId: 'asdasdasd',
      Accept: 'application/json'
    }
  })
    .then(res => {
      commit('updateUserData', res.data.data)
      commit('updateTicketId', res.data.data.ticket_id)
    })
    .catch(() => {
      dispatch('logout')
    })
}
