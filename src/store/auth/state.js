export default function () {
  return {
    userData: '',
    loggedIn: JSON.stringify(false),
    user_id: JSON.stringify(0),
    ticket_id: JSON.stringify(0),
    sessionId: '',
    bearerToken: ''
  }
}
