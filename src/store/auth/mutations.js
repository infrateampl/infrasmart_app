export const updateUserData = (state, val) => {
  state.userData = JSON.stringify(val)
  window.localStorage.setItem('userData', JSON.stringify(val))
}
export const updateUserId = (state, val) => {
  state.user_id = JSON.stringify(val)
  window.localStorage.setItem('user_id', JSON.stringify(val))
}
export const updateTicketId = (state, val) => {
  state.ticket_id = JSON.stringify(val)
  window.localStorage.setItem('ticket_id', JSON.stringify(val))
}
export const setLoggedIn = (state) => {
  state.loggedIn = JSON.stringify(true)
  window.localStorage.setItem('loggedIn', JSON.stringify(true))
}
export const setSessionId = (state, value) => {
  state.sessionId = JSON.stringify(value)
  window.localStorage.setItem('sessionId', JSON.stringify(value))
}
export const setBearerToken = (state, value) => {
  state.bearerToken = JSON.stringify(value)
  window.localStorage.setItem('bearerToken', JSON.stringify(value))
}
export const setLoggedOut = (state) => {
  state.loggedIn = JSON.stringify(false)
  window.localStorage.clear()
  window.localStorage.setItem('loggedIn', JSON.stringify(false))
}
