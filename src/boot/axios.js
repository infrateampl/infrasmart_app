import { boot } from 'quasar/wrappers'
import axios from 'axios'

const api = axios.create({
  baseURL: process.env.API_URL,
  timeout: 5000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Accept-Language': JSON.parse(window.localStorage.getItem('lang') ?? '"pl"'),
    Accept: 'application/json'
  }
})

export default boot(({ app }) => {
  app.config.globalProperties.$axios = axios
  app.config.globalProperties.$api = api
})

export { axios, api }
